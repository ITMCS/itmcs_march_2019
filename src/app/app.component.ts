/**
 *
 * @author ITMusketeers
 * Created On  7th March 2019
 */

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {  
  array = ['1.jpg', '2.jpg','3.jpg', '4.jpg','5.jpg','6.jpg', '7.jpg','8.jpg', '9.jpg','10.jpg','11.jpg', '12.jpg','13.jpg', '14.jpg','15.jpg','16.jpg', '17.jpg','18.jpg', '19.jpg','20.jpg'];
  cnt=0;
  sorted =new Array();
  data=null;
  rank=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  public show:boolean = false;
  public buttonName:any = 'Show';

  // This Method is created for pick Random First Images
  randomImg(array) {      
      var num = Math.floor( Math.random() * array.length );
      var img = array[ num ];
      return img;
    }
   imgOne=this.randomImg(this.array);

   // This Method is created for pick Random second Image
   randomImgTwo(array) {      
    var num = Math.floor( Math.random() * array.length );
    var img = array[ num ];
    return img;
  }
  imgTwo=this.randomImgTwo(this.array);

   // This Method is created for reshuffle Images
  reshuffle()
  {
    this.imgOne=this.randomImg(this.array);
    this.imgTwo=this.randomImgTwo(this.array);
  }

  // This Method is created Increase Ranking of Images when user click on first Image
  public showSmiley:boolean = false;
  public name:any = 'Show';
  smiley="smiley1.jpg";

  clickOne()
  {
    var index=this.array.indexOf(this.imgOne);
    var old =this.rank[index];
    this.rank[index] =old +1;

    this.cnt++;
    if( this.cnt==10)
       this.display();

    //Logic For Hide and Show 
    this.showSmiley = !this.showSmiley;
    if(this.showSmiley)  
      this.name = "Hide";
    else
      this.name = "Show";

    // Logic for Display smiley
    if(this.smiley=="smiley1.jpg")
      this.smiley="smiley2.jpg"
    else if(this.smiley=="smiley2.jpg")
      this.smiley="smiley3.jpg"
    else
      this.smiley="smiley1.jpg"

    //Shuffle Images   
    this.imgOne=this.randomImg(this.array);
    this.imgTwo=this.randomImgTwo(this.array); 
  }

  // This Method is created Increase Ranking of Images when user click on Second Image
  clickTwo()
  {
    var index=this.array.indexOf(this.imgTwo);
    var old =this.rank[index];
    this.rank[index] =old +1;
    this.cnt++;
    if( this.cnt==10)
      this.display();
    
    //Logic For Hide and Show 
    this.showSmiley = !this.showSmiley;
    if(this.showSmiley)  
      this.name = "Hide";
    else
      this.name = "Show";

    // Logic for Display smiley
    if(this.smiley=="smiley1.jpg")
      this.smiley="smiley2.jpg"
    else if(this.smiley=="smiley2.jpg")
      this.smiley="smiley3.jpg"
    else
      this.smiley="smiley1.jpg"
    
    //Shuffle Images   
    this.imgOne=this.randomImg(this.array);
    this.imgTwo=this.randomImgTwo(this.array);
  }

  // This Method is created Display Top Five Images who got high Ranking
  highRankedImg =null;
  display()
  {  
    var max =Math.max.apply(null, this.rank);
    var index=this.rank.indexOf(max);
    this.highRankedImg =this.array[index];

    //Logic For Hide and Show
    this.show = !this.show;
    if(this.show)  
      this.buttonName = "Hide";
    else
      this.buttonName = "Show";
  }
}
